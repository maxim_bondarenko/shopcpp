#include <iostream>
#include "category.h"
#include "item.h"

using namespace std;

Category::Category(const string &name) : MyIdentifiable(name) {}

Category::~Category() {
	//cout << "Destructing category " << getName() << endl;
}

Item &Category::getItem(int index) {
    return getConnected(index);
}

int Category::itemCount() {
    return getConnectionsCount();
}

void Category::removeItem(int index) {
    disconnect(getConnected(index));
}


void Category::print(ostream &stream, bool printItems) {
    stream << "Cat: " << getName() << endl;
    // Identifiable::print(stream);
	// stream << "\n";
	int count = itemCount();
    if ( printItems && count != 0 ) {
		for ( int i = 0; i < count; i++ ) {
            stream << "* ";
			getItem(i).print(stream, false);
            //stream << "\n";
        }
    }
}
