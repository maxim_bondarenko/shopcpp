#ifndef CATEGORY_H
#define CATEGORY_H

#include <ostream>
#include "Connectable.h"
#include "Identifiable.h"
#include "Registerable.h"

using namespace std;

class Item;

class Category: public MultiConnectable<Item, Category>,
                public Identifiable<Category>,
                public Registerable<Category>
{
    typedef Identifiable<Category> MyIdentifiable;
public:
    Category(const string &name);
	~Category();
	
    Item &getItem(int index); 

    int itemCount(); 

    void removeItem(int index); 

    void print(ostream &stream, bool printItems = true); 
};

#endif // CATEGORY_H
