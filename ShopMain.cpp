#include <iostream>
#include <vector>

#include "Registerable.h"
#include "Identifiable.h"
#include "Connectable.h"

#include "category.h"
#include "item.h"
#include "order.h"
#include "customer.h"

using namespace std;

int main() {
    typedef std::vector<Category*> Categories;
	typedef std::vector<Customer*> Customers;
	
	Categories categories;
	Customers customers;

	categories.push_back(new Category("Hardcore books"));
	Category *c2 = new Category("Unix");
	categories.push_back(c2);
	Item *i1 = new Item("Linux for cranes", *c2);
	Item *i2 = new Item("FreeBSD is easy", *c2);
	Customer *vasia = new Customer("Vasia");
	Order *o1 = new Order("", *vasia);
	o1->addItem(*i1);
	o1->addItem(*i2);
	customers.push_back(vasia);
	Customer* petya = new Customer("Petya");
	Order *o2 = new Order("", *petya);
	o2->addItem(*i1);
	customers.push_back(petya);

	for ( Categories::iterator it = categories.begin(); it != categories.end(); ++it ) {
		(*it)->print(cout, true);
	}
	cout << endl;
	for ( int i = 0, count = Item::getObjectCount(); i < count; i++ ) {
		Item::getObject(i).print(cout, false);
	}
	cout << endl;
	o1->print(cout, true);
	o2->print(cout, true);
	cout << endl;
	for ( int i = 0, count = Item::getObjectCount(); i < count; i++ ) {
		Item::getObject(i).print(cout, true);
	}
	
	cout << endl;

	for ( Categories::iterator it = categories.begin(); it != categories.end(); ++it ) {
		delete *it;
	}
	for ( Customers::iterator it = customers.begin(); it != customers.end(); ++it ) {
		delete *it;
	}
	
    return 0;
}
