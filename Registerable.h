#ifndef REGISTERABLE_H
#define REGISTERABLE_H

#include <vector>
#include <algorithm>
#include <exception>

using namespace std;

class RegisterableError: public std::exception {};
class RegisterableNotInStorage: public RegisterableError {};
class OutOfContainerBounds: public RegisterableError {};

// Class implements Registerable behaviour, so that you always can get
// count of created insnances of class and get any instance by index.
// To use it, client should inherite Registerable and put self type as 
// template parameter.
template <typename Descendant> class Registerable {
public:
	typedef std::vector<Descendant*> StorageType;

	Registerable() {
		Register();
	}

	Registerable(const Registerable &other) {
		Register();
	}
	
	virtual ~Registerable() {
		typename StorageType::iterator thisInStorage = std::find(objects.begin(), objects.end(), this);
		if ( thisInStorage != objects.end() ) {
			objects.erase( thisInStorage );
		} else {
			throw RegisterableNotInStorage();
		}
	}

	static int getObjectCount() {
		return objects.size();
	}
	
	static Descendant &getObject(int objectIndex) {
		if ( objectIndex >= objects.size() ) {
			throw OutOfContainerBounds();
		}
		
		return *(objects[objectIndex]);
	}
private:
	void Register() {
		objects.push_back(static_cast<Descendant*>(this));
	}
	
	static StorageType objects;
};

template <class T> typename Registerable<T>::StorageType Registerable<T>::objects;

#endif // REGISTERABLE_H
