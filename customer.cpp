#include "customer.h"

#include "order.h"

Customer::Customer(const string &name) : MyIdentifiable(name) {}

Customer::~Customer() {
	//cout << "Destructing customer " << getName() << endl;
}

int Customer::orderCount() {
    return getConnectionsCount();
}

Order &Customer::getOrder(int index) {
    return getConnected(index);
}
