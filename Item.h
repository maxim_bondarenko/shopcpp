#ifndef ITEM_H
#define ITEM_H

#include "Connectable.h"
#include "Identifiable.h"
#include "Registerable.h"

#include "category.h"
#include "order.h"

using namespace std;

class Category;
class Order;

class Item: public SingleConnectable<Category, Item>,
            public MultiConnectable<Order, Item>,
            public Identifiable<Item>,
            public Registerable<Item>
{
    typedef SingleConnectable<Category, Item> CategoryConnectable;
    typedef MultiConnectable<Order, Item> OrderConnectable;
    typedef Identifiable<Item> MyIdentifiable;
public:
    Item(const string &name, Category& category);
	~Item();

    void changeCategory(Category &newCategory);

    Category &getCategory();

    int orderCount(); 

    Order &getOrder(int index);

	void print(ostream &stream, bool printOrders = true);
};


#endif // ITEM_H
