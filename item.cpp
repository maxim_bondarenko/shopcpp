#include "item.h"
#include "category.h"
#include "order.h"

using namespace std;

Item::Item(const string &name, Category& category) : CategoryConnectable(category), MyIdentifiable(name) {}

Item::~Item() {
	//cout << "Destructing item " << getName() << endl;
}

void Item::changeCategory(Category &newCategory) {
    CategoryConnectable::connect(newCategory);
}

Category &Item::getCategory() {
    return CategoryConnectable::getConnected();
}

int Item::orderCount() {
    return OrderConnectable::getConnectionsCount();
}

Order &Item::getOrder(int index) {
    return OrderConnectable::getConnected(index);
}

void Item::print(ostream &stream, bool printOrders) {
	stream << "Item: " << getName() << " (" << getCategory().getName() << ")\n";

	if ( printOrders ) {
		int count = orderCount();
		for ( int i = 0; i < count; i++ ) {
			stream << "* ";
			getOrder(i).print(stream, false);
		}
	}
}
