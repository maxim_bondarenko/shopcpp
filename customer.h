#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <ostream>
#include <string>
#include "Connectable.h"
#include "Identifiable.h"
#include "Registerable.h"

using namespace std;

class Order;

class Customer: public MultiConnectable<Order, Customer>,
                public Identifiable<Customer>,
                public Registerable<Customer>
{
    typedef Identifiable<Customer> MyIdentifiable;
public:
    Customer(const string &name);
	~Customer();

    int orderCount();

    Order &getOrder(int index); 
};

#endif // CUSTOMER_H
