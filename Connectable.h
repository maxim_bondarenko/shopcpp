#ifndef CONNECTABLE_H
#define CONNECTABLE_H

#include <exception>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

class ConnectionError: public std::exception {};
class AlreadyConnected: public ConnectionError {};
class NotConnected: public ConnectionError {};

// Connectable family implements conectable behaviour for descendant class.
// Connectable itself is abstract, use SingleConnectable and MultiConnectable instead
template <typename TargetType, typename SelfType> class Connectable {
protected:
	typedef Connectable<SelfType, TargetType> Target;
public:
	virtual ~Connectable() {}
    virtual void connect(Target &target, bool notifyTarget = true) {
        connect(static_cast<TargetType&>(target), notifyTarget);
    }

    virtual void connect(TargetType &target, bool notifyTarget = true) = 0;

    virtual void disconnect(Target &target, bool notifyTarget = true) {
        disconnect(static_cast<TargetType&>(target), notifyTarget);
    }

    virtual void disconnect(TargetType &target, bool notifyTarget = true) = 0;
};
	
// SingleConnectable
// ATTENTION: then Connect called, disconnects from previous connection
template <typename TargetType, typename SelfType> class SingleConnectable :
		public Connectable<TargetType, SelfType> {
protected:
    typedef Connectable<TargetType, SelfType> Base;
    typedef Connectable<SelfType, TargetType> Target;
public:
    SingleConnectable(TargetType &target) : connected(&target) {
        target.Target::connect(*this, false);
	}
	
	~SingleConnectable() {
        if ( connected != NULL ) {
			connected->Target::disconnect(*this, false);
        }
	}
	
    virtual void connect(TargetType &target, bool notifyTarget = true) {
		//cout << "In SingleConnectable::connect\n";
		if ( connected == &target ) {
			throw AlreadyConnected();
		}
		connected->Target::disconnect(*this, false);
		if ( notifyTarget ) {
			target.Target::connect(*this, false);
		}
	}
	
	virtual void disconnect(TargetType &target, bool notifyTarget = true) {
		//cout << "In SingleConnectable::disconnect\n";
		if ( connected != &target ) {
			throw NotConnected();
		}
		if ( notifyTarget ) {
			target.Target::disconnect(*this, false);
		}
		connected = NULL;
		delete this;
	}

    TargetType &getConnected() {
        return *connected;
    }
private:
    TargetType* connected;
};

// MultiConnectable
template <typename TargetType, typename SelfType> class MultiConnectable :
		public Connectable<TargetType, SelfType> {
protected:
    typedef Connectable<TargetType, SelfType> Base;
    typedef Connectable<SelfType, TargetType> Target;
private:
    typedef std::vector<TargetType*> ConnectionsStorage;
public:
    ~MultiConnectable() {
		typename ConnectionsStorage::iterator it;
		for (it = connections.begin(); it != connections.end(); ++it) {
            (**it).Target::disconnect(*this, false);
		}
	}

    virtual void connect(TargetType &target, bool notifyTarget = true) {
		//cout << "It MultiConnectable::connect\n";
		typename ConnectionsStorage::iterator it = 
				std::find(connections.begin(), connections.end(), &target);
		if ( it != connections.end() ) {
			throw AlreadyConnected();
		}
		connections.push_back(&target);
		if ( notifyTarget ) {
			target.Target::connect(*this, false);
		}
	}
	
    virtual void disconnect(TargetType &target, bool notifyTarget = true) {
		//cout << "In MultiConnectable::disconnect\n";
		typename ConnectionsStorage::iterator it = 
				std::find(connections.begin(), connections.end(), &target);
		if ( it == connections.end() ) {
			throw NotConnected();
		}
		connections.erase(it);
		if ( notifyTarget ) {
			target.Target::disconnect(*this, false);
		}
	}

    int getConnectionsCount() {
        return connections.size();
    }

    TargetType &getConnected(int index) {
        return *(connections[index]);
    }
private:
	ConnectionsStorage connections;
};
	

#endif // CONNECTABLE_H
