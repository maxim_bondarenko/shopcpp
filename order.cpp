#include "order.h"

#include "item.h"
#include "customer.h"

Order::Order(const string &name, Customer &customer) : CustomerConnectable(customer), MyIdentifiable(name) {}

Order::~Order() {
	//cout << "Destructing order " << getID() << endl;
}

Customer &Order::getCustomer() {
    return CustomerConnectable::getConnected();
}

int Order::itemCount() {
    return ItemConnectable::getConnectionsCount();
}

Item &Order::getItem(int index) {
    return ItemConnectable::getConnected(index);
}

void Order::addItem(Item &newItem) {
    ItemConnectable::connect(newItem);
}

void Order::print(ostream &stream, bool printItems) {
    stream << "Order: " << getID() << " by " << CustomerConnectable::getConnected().getName() << endl;
 
	if ( printItems ) {
		int count = itemCount();
		for ( int i = 0; i < count; i++ ) {
			stream << "* ";
			getItem(i).print(stream, false);
			//stream << "\n";
		}
	}
}
