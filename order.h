#ifndef ORDER_H
#define ORDER_H

#include "Connectable.h"
#include "Identifiable.h"
#include "Registerable.h"

#include "customer.h"
#include "Item.h"

using namespace std;

class Item;
class Customer;

class Order: public MultiConnectable<Item, Order>,
             public SingleConnectable<Customer, Order>,
            public Identifiable<Order>,
            public Registerable<Order>
{
    typedef SingleConnectable<Customer, Order> CustomerConnectable;
    typedef MultiConnectable<Item, Order> ItemConnectable;
    typedef Identifiable<Order> MyIdentifiable;
public:
    Order(const string &name, Customer &customer);
	~Order();

    Customer &getCustomer();

    void addItem(Item &newItem);

    int itemCount(); 

    Item &getItem(int index); 

    void print(ostream &stream, bool printItems = true);
};

#endif // ORDER_H
