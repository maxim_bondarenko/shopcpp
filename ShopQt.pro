#-------------------------------------------------
#
# Project created by QtCreator 2012-03-02T16:42:54
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = ShopQt
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    order.cpp \
    item.cpp \
    customer.cpp \
    category.cpp

HEADERS += \
    Registerable.h \
    order.h \
    ListHolder.h \
    Item.h \
    Identifiable.h \
    customer.h \
    Connectable.h \
    Category.h
