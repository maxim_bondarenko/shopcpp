#ifndef IDENTIFIABLE_H
#define IDENTIFIABLE_H

#include <iostream>

using namespace std;

// Class adds identifiable behaviour to it's descendant, which means, any object
// of descendant class will have unique ID and, optionally, name
template <typename T> class Identifiable {
public:
    Identifiable(const std::string &objName = "") : name(objName) {
		ID = generateID();
	}
	
	Identifiable(const Identifiable &other) {
		ID = generateID();
	}
	
	~Identifiable() {}

	const Identifiable &operator =(const Identifiable &other) {}
	
	int getID() const {
		return ID;
	}
	
	string getName() const {
		return name;
	}
	
    void print(ostream &stream) {
        stream << "ID: " << getID() << ", Name: " << name.c_str();
	}
private:
	int ID;
	string name;
	static int lastID;
	
	int generateID() {
		lastID += 1;
		return lastID;
	}
};

template <typename T> int Identifiable<T>::lastID = 0;

#endif // IDENTIFIABLE_H
