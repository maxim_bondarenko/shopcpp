#include <iostream>

#include "Registerable.h"
#include "Identifiable.h"
#include "Connectable.h"

#include "category.h"
#include "item.h"
#include "order.h"
#include "customer.h"

using namespace std;

int main() {
    std::vector<Category*> categories;
    std::vector<Order*> orders;
    std::vector<Customer*> customers;

    Category *c1 = new Category("Phones");
    Item *i1 = new Item("nokia", *c1);
    Item *i2 = new Item("Samsung", *c1);
    Customer* cus = new Customer("Oleg");
    Order *o1 = new Order("first", *cus);
    o1->addItem(*i1);

    c1->print(std::cout);
    o1->print(std::cout);

    cout << "\n\nhere\n";

    delete c1;

    o1->print(std::cout);

    cout << "Here\n";

    return 0;
}
